/* SEAWATER ROUTINES & OTHER FOR FLOAT CONTROL PROGRAM */
#include <math.h>
#include <stdio.h>
#include "ballast.h"    /*  This contains function & structure definitions */
double sw_dens(double S, double T, double P)
{
/*
% SW_DENS    Density of sea water
%=========================================================================
% SW_DENS  $Revision: 1.4 $  $Date: 2003/03/06 22:02:34 $
%          Copyright (C) CSIRO, Phil Morgan 1992.
%
% USAGE:  dens = sw_dens(S,T,P)
%
% DESCRIPTION:
%    Density of Sea Water using UNESCO 1983 (EOS 80) polynomial.
%
% INPUT:  (all must have same dimensions)
%   S = salinity    [psu      (PSS-78)]
%   T = temperature [degree C (IPTS-68)]
%   P = pressure    [db]
%       (P may have dims 1x1, mx1, 1xn or mxn for S(mxn) )
%
% OUTPUT:
%   dens = density  [kg/m^3] 
% 
% AUTHOR:  Phil Morgan 92-11-05  (morgan@ml.csiro.au)
%
% DISCLAIMER:
%   This software is provided "as is" without warranty of any kind.  
%   See the file sw_copy.m for conditions of use and licence.
%
% REFERENCES:
%    Fofonoff, P. and Millard, R.C. Jr
%    Unesco 1983. Algorithms for computation of fundamental properties of 
%    seawater, 1983. _Unesco Tech. Pap. in Mar. Sci._, No. 44, 53 pp.
%
%    Millero, F.J., Chen, C.T., Bradshaw, A., and Schleicher, K.
%    " A new high pressure equation of state for seawater"
%    Deap-Sea Research., 1980, Vol27A, pp255-264.
%=========================================================================
*/
double densPO, K, dens;
double sw_dens0(double S, double T);
double sw_seck(double S, double T, double P);
densPO= sw_dens0(S,T);
K      = sw_seck(S,T,P);
P      = P/10;   /* convert from db to atm pressure units */
dens   = densPO/(1-P/K);
return dens;
}
/*-------------------------------------------------------------------*/
double sw_dens0(double S, double T)
{
/*
% SW_DENS0   Denisty of sea water at atmospheric pressure
%=========================================================================
% SW_DENS0  $Revision: 1.4 $  $Date: 2003/03/06 22:02:34 $
%           Copyright (C) CSIRO, Phil Morgan 1992
%
% USAGE:  dens0 = sw_dens0(S,T)
%
% DESCRIPTION:
%    Density of Sea Water at atmospheric pressure using
%    UNESCO 1983 (EOS 1980) polynomial.
%
% INPUT:  (all must have same dimensions)
%   S = salinity    [psu      (PSS-78)]
%   T = temperature [degree C (IPTS-68)]
%
% OUTPUT:
%   dens0 = density  [kg/m^3] of salt water with properties S,T,
%           P=0 (0 db gauge pressure)
% 
% AUTHOR:  Phil Morgan 92-11-05  (morgan@ml.csiro.au)
%
% DISCLAIMER:
%   This software is provided "as is" without warranty of any kind.  
%   See the file sw_copy.m for conditions of use and licence.
%
% REFERENCES:
%     Unesco 1983. Algorithms for computation of fundamental properties of 
%     seawater, 1983. _Unesco Tech. Pap. in Mar. Sci._, No. 44, 53 pp.
%
%     Millero, F.J. and  Poisson, A.
%     International one-atmosphere equation of state of seawater.
%     Deep-Sea Res. 1981. Vol28A(6) pp625-629.
%=========================================================================
*/
/*    UNESCO 1983 eqn(13) p17.  */
double b0,b1,b2,b3,b4,c0,c1,c2,d0;
double dens;
double sw_smow(double T);
b0 =  8.24493e-1;
b1 = -4.0899e-3;
b2 =  7.6438e-5;
b3 = -8.2467e-7;
b4 =  5.3875e-9;
c0 = -5.72466e-3;
c1 = +1.0227e-4;
c2 = -1.6546e-6;
d0 = 4.8314e-4;
/*%$$$ dens = sw_smow(T) + (b0 + b1*T + b2*T.^2 + b3*T.^3 + b4*T.^4).*S  ...
%$$$                    + (c0 + c1*T + c2*T.^2).*S.*sqrt(S) + d0*S.^2;
*/
dens = sw_smow(T) + (b0 + (b1 + (b2 + (b3 + b4*T)*T)*T)*T)*S
                   + (c0 + (c1 + c2*T)*T)*S*sqrt(S) + d0*S*S;	       
return dens;
}
/*-------------------------------------------------------------------*/
double sw_seck(double S, double T, double P)
{
/*
% SW_SECK    Secant bulk modulus (K) of sea water
%=========================================================================
% SW_SECK  $Revision: 1.4 $  $Date: 2003/03/06 22:02:34 $
%          Copyright (C) CSIRO, Phil Morgan 1992.
%
% USAGE:  dens = sw_seck(S,T,P)
%
% DESCRIPTION:
%    Secant Bulk Modulus (K) of Sea Water using Equation of state 1980. 
%    UNESCO polynomial implementation.
%
% INPUT:  (all must have same dimensions)
%   S = salinity    [psu      (PSS-78) ]
%   T = temperature [degree C (IPTS-68)]
%   P = pressure    [db]
%       (alternatively, may have dimensions 1*1 or 1*n where n is columns in S)
%
% OUTPUT:
%   K = Secant Bulk Modulus  [bars]
% 
% AUTHOR:  Phil Morgan 92-11-05  (morgan@ml.csiro.au)
%
% DISCLAIMER:
%   This software is provided "as is" without warranty of any kind.  
%   See the file sw_copy.m for conditions of use and licence.
%
% REFERENCES:
%    Fofonoff, P. and Millard, R.C. Jr
%    Unesco 1983. Algorithms for computation of fundamental properties of 
%    seawater, 1983. _Unesco Tech. Pap. in Mar. Sci._, No. 44, 53 pp.
%    Eqn.(15) p.18
%
%    Millero, F.J. and  Poisson, A.
%    International one-atmosphere equation of state of seawater.
%    Deep-Sea Res. 1981. Vol28A(6) pp625-629.
%=========================================================================
*/
/*
%--------------------------------------------------------------------
% COMPUTE COMPRESSION TERMS
%--------------------------------------------------------------------
*/
double h0,h1,h2,h3,AW,k0,k1,k2,BW,e0,e1,e2,e3,e4,KW;
double j0,i2,i1,i0,SR,A,m0,m1,m2,B;
double f0,f1,f2,f3,g0,g1,g2,K0,K;
P = P/10;  /* convert from db to atmospheric pressure units
% Pure water terms of the secant bulk modulus at atmos pressure.
% UNESCO eqn 19 p 18 */
h3 = -5.77905E-7;
h2 = +1.16092E-4;
h1 = +1.43713E-3;
h0 = +3.239908;   /* %[-0.1194975];
%AW = h0 + h1*T + h2*T^2 + h3*T^3; */
AW  = h0 + (h1 + (h2 + h3*T)*T)*T;
k2 =  5.2787E-8;
k1 = -6.12293E-6;
k0 =  +8.50935E-5;   /* %[+3.47718E-5]; */
BW  = k0 + (k1 + k2*T)*T;
/*%BW = k0 + k1*T + k2*T^2; */
e4 = -5.155288E-5;
e3 = +1.360477E-2;
e2 = -2.327105;
e1 = +148.4206;
e0 = 19652.21;   /*  %[-1930.06]; */
KW  = e0 + (e1 + (e2 + (e3 + e4*T)*T)*T)*T;  /*  % eqn 19 
%KW = e0 + e1*T + e2*T^2 + e3*T^3 + e4*T^4;
%--------------------------------------------------------------------
% SEA WATER TERMS OF SECANT BULK MODULUS AT ATMOS PRESSURE.
%--------------------------------------------------------------------
*/
j0 = 1.91075E-4;
       
i2 = -1.6078E-6;
i1 = -1.0981E-5;
i0 =  2.2838E-3;
SR = sqrt(S);
A  = AW + (i0 + (i1 + i2*T)*T + j0*SR)*S; 
/*A = AW + (i0 + i1*T + i2*T^2 + j0*SR)*S;   % eqn 17 */
  
m2 =  9.1697E-10;
m1 = +2.0816E-8;
m0 = -9.9348E-7;
B = BW + (m0 + (m1 + m2*T)*T)*S;  /*  % eqn 18
%B  = BW + (m0 + m1*T + m2*T^2)*S;   % eqn 18 */
f3 =  -6.1670E-5;
f2 =  +1.09987E-2;
f1 =  -0.603459;
f0 = +54.6746;
g2 = -5.3009E-4;
g1 = +1.6483E-2;
g0 = +7.944E-2;
K0 = KW + (  f0 + (f1 + (f2 + f3*T)*T)*T
        +   (g0 + (g1 + g2*T)*T)*SR         )*S;    /*  % eqn 16
%K0  = KW + (f0 + f1*T + f2*T^2 + f3*T^3)*S ...
%         + (g0 + g1*T + g2*T^2)*SR*S;             % eqn 16 */
K = K0 + (A + B*P)*P;  /* % eqn 15  */
return K;
}
/*%----------------------------------------------------------------------------
*/
double sw_smow( double T)
{
/*% SW_SMOW    Denisty of standard mean ocean water (pure water)
%=========================================================================
% SW_SMOW  $Revision: 1.4 $  $Date: 2003/03/06 22:02:34 $
%          Copyright (C) CSIRO, Phil Morgan 1992.
%
% USAGE:  dens = sw_smow(T)
%
% DESCRIPTION:
%    Denisty of Standard Mean Ocean Water (Pure Water) using EOS 1980. 
%
% INPUT: 
%   T = temperature [degree C (IPTS-68)]
%
% OUTPUT:
%   dens = density  [kg/m^3] 
% 
% AUTHOR:  Phil Morgan 92-11-05  (morgan@ml.csiro.au)
%
% DISCLAIMER:
%   This software is provided "as is" without warranty of any kind.  
%   See the file sw_copy.m for conditions of use and licence.
%
% REFERENCES:
%     Unesco 1983. Algorithms for computation of fundamental properties of 
%     seawater, 1983. _Unesco Tech. Pap. in Mar. Sci._, No. 44, 53 pp.
%     UNESCO 1983 p17  Eqn(14)
%
%     Millero, F.J & Poisson, A.
%     INternational one-atmosphere equation of state for seawater.
%     Deep-Sea Research Vol28A No.6. 1981 625-629.    Eqn (6)
%=========================================================================
*/
double a0,a1,a2,a3,a4,a5,dens;
a0 = 999.842594;
a1 =   6.793952e-2;
a2 =  -9.095290e-3;
a3 =   1.001685e-4;
a4 =  -1.120083e-6;
a5 =   6.536332e-9;
dens = a0 + (a1 + (a2 + (a3 + (a4 + a5*T)*T)*T)*T)*T;
return dens;
}
double sw_pden(double S, double T, double P, double PR)
{
/* % SW_PDEN    Potential density
%===========================================================================
% SW_PDEN  $Revision: 1.4 $  $Date: 2003/03/06 22:02:34 $
%          Copyright (C) CSIRO, Phil Morgan  1992. 
%
% USAGE:  pden = sw_pden(S,T,P,PR) 
%
% DESCRIPTION:
%    Calculates potential density of water mass relative to the specified
%    reference pressure by pden = sw_dens(S,ptmp,PR).
%   
% INPUT:  (all must have same dimensions)
%   S  = salinity    [psu      (PSS-78) ]
%   T  = temperature [degree C (IPTS-68)]
%   P  = pressure    [db]
%   PR = Reference pressure  [db]
%       (P may have dims 1x1, mx1, 1xn or mxn for S(mxn) )
%
% OUTPUT:
%   pden = Potential denisty relative to the ref. pressure [kg/m^3] 
%
% AUTHOR:  Phil Morgan 1992/04/06  (morgan@ml.csiro.au)
%
% DISCLAIMER:
%   This software is provided "as is" without warranty of any kind.  
%   See the file sw_copy.m for conditions of use and licence.
%
% REFERENCES:
%   A.E. Gill 1982. p.54
%   "Atmosphere-Ocean Dynamics"
%   Academic Press: New York.  ISBN: 0-12-283522-0
%=========================================================================
*/
double pden,ptmp;
ptmp = sw_ptmp(S,T,P,PR);
pden = sw_dens(S,ptmp,PR);
return   pden; 
}  
/*%=========================================================================
*/
double sw_ptmp(double S,double T,double P,double PR)
{
/*% SW_PTMP    Potential temperature
%===========================================================================
% SW_PTMP  $Revision: 1.4 $  $Date: 2003/03/06 22:02:34 $
%          Copyright (C) CSIRO, Phil Morgan 1992. 
%
% USAGE:  ptmp = sw_ptmp(S,T,P,PR) 
%
% DESCRIPTION:
%    Calculates potential temperature as per UNESCO 1983 report.
%   
% INPUT:  (all must have same dimensions)
%   S  = salinity    [psu      (PSS-78) ]
%   T  = temperature [degree C (IPTS-68)]
%   P  = pressure    [db]
%   PR = Reference pressure  [db]
%        (P & PR may have dims 1x1, mx1, 1xn or mxn for S(mxn) )
%
% OUTPUT:
%   ptmp = Potential temperature relative to PR [degree C (IPTS-68)]
%
% AUTHOR:  Phil Morgan 92-04-06  (morgan@ml.csiro.au)
%
% DISCLAIMER:
%   This software is provided "as is" without warranty of any kind.  
%   See the file sw_copy.m for conditions of use and licence.
%
% REFERENCES:
%    Fofonoff, P. and Millard, R.C. Jr
%    Unesco 1983. Algorithms for computation of fundamental properties of 
%    seawater, 1983. _Unesco Tech. Pap. in Mar. Sci._, No. 44, 53 pp.
%    Eqn.(31) p.39
%
%    Bryden, H. 1973.
%    "New Polynomials for thermal expansion, adiabatic temperature gradient
%    and potential temperature of sea water."
%    DEEP-SEA RES., 1973, Vol20,401-408.
%=========================================================================
*/
double del_P,del_th,th,q,PT;
/* % theta1 */
del_P  = PR - P;
del_th = del_P*sw_adtg(S,T,P);
th     = T + 0.5*del_th;
q      = del_th;
/*% theta2 */
del_th = del_P*sw_adtg(S,th,P+0.5*del_P);
th     = th + (1 - 1/sqrt(2))*(del_th - q);
q      = (2-sqrt(2))*del_th + (-2+3/sqrt(2))*q;
/* % theta3 */
del_th = del_P*sw_adtg(S,th,P+0.5*del_P);
th     = th + (1 + 1/sqrt(2))*(del_th - q);
q      = (2 + sqrt(2))*del_th + (-2-3/sqrt(2))*q;
/*% theta4 */
del_th = del_P*sw_adtg(S,th,P+del_P);
PT     = th + (del_th - 2*q)/6;
return     PT;
/* %=========================================================================
*/
}
double  sw_adtg(double S,double T, double P)
{
/*% SW_ADTG    Adiabatic temperature gradient
%===========================================================================
% SW_ADTG   $Revision: 1.4 $  $Date: 2003/03/06 22:02:34 $
%           Copyright (C) CSIRO, Phil Morgan  1992.
%
% adtg = sw_adtg(S,T,P)
%
% DESCRIPTION:
%    Calculates adiabatic temperature gradient as per UNESCO 1983 routines.
%
% INPUT:  (all must have same dimensions)
%   S = salinity    [psu      (PSS-78) ]
%   T = temperature [degree C (IPTS-68)]
%   P = pressure    [db]
%       (P may have dims 1x1, mx1, 1xn or mxn for S(mxn) )
%
% OUTPUT:
%   ADTG = adiabatic temperature gradient [degree_C/db]
%
% AUTHOR:  Phil Morgan 92-04-03  (morgan@ml.csiro.au)
%
% DISCLAIMER:
%   This software is provided "as is" without warranty of any kind.  
%   See the file sw_copy.m for conditions of use and licence.
%
% REFERENCES:
%    Fofonoff, P. and Millard, R.C. Jr
%    Unesco 1983. Algorithms for computation of fundamental properties of 
%    seawater. Unesco Tech. Pap. in Mar. Sci., No. 44, 53 pp.  Eqn.(31) p.39
%
%    Bryden, H. 1973.
%    "New Polynomials for thermal expansion, adiabatic temperature gradient
%    and potential temperature of sea water."
%    DEEP-SEA RES., 1973, Vol20,401-408.
%=========================================================================
*/
double a0,a1,a2,a3,b0,b1,c0,c1,c2,c3,d0,d1,e0,e1,e2,ADTG;
a0 =  3.5803E-5;
a1 = +8.5258E-6;
a2 = -6.836E-8;
a3 =  6.6228E-10;
b0 = +1.8932E-6;
b1 = -4.2393E-8;
c0 = +1.8741E-8;
c1 = -6.7795E-10;
c2 = +8.733E-12;
c3 = -5.4481E-14;
d0 = -1.1351E-10;
d1 =  2.7759E-12;
e0 = -4.6206E-13;
e1 = +1.8676E-14;
e2 = -2.1687E-16;
ADTG =      a0 + (a1 + (a2 + a3*T)*T)*T 
         + (b0 + b1*T)*(S-35)
	 + ( (c0 + (c1 + (c2 + c3*T)*T)*T) + (d0 + d1*T)*(S-35) )*P
         + (  e0 + (e1 + e2*T)*T )*P*P;
return ADTG;
}
/*%==========================================================================
*/
double sw_temp(double S,double T, double P, double PR)
{
/*% SW_TEMP    Temperature from potential temperature
%===========================================================================
% TEMP  $Revision: 1.4 $  $Date: 2003/03/06 22:02:34 $
%       Copyright (C) CSIRO, Phil Morgan  1992. 
%
% USAGE:  temp = sw_temp(S,PTMP,P,PR) 
%
% DESCRIPTION:
%    Calculates temperature from potential temperature at the reference
%    pressure PR and in-situ pressure P.
%   
% INPUT:  (all must have same dimensions)
%   S     = salinity              [psu      (PSS-78) ]
%   PTMP  = potential temperature [degree C (IPTS-68)]
%   P     = pressure              [db]
%   PR    = Reference pressure    [db]
%           (P may have dims 1x1, mx1, 1xn or mxn for S(mxn) )
%
% OUTPUT:
%   temp = temperature [degree C (IPTS-68)]
%
% AUTHOR:  Phil Morgan 92-04-06  (morgan@ml.csiro.au)
%
% DISCLAIMER:
%   This software is provided "as is" without warranty of any kind.  
%   See the file sw_copy.m for conditions of use and licence.
%
% REFERENCES:
%    Fofonoff, P. and Millard, R.C. Jr
%    Unesco 1983. Algorithms for computation of fundamental properties of 
%    seawater, 1983. _Unesco Tech. Pap. in Mar. Sci._, No. 44, 53 pp.
%    Eqn.(31) p.39
%
%    Bryden, H. 1973.
%    "New Polynomials for thermal expansion, adiabatic temperature gradient
%    and potential temperature of sea water."
%    DEEP-SEA RES., 1973, Vol20,401-408.
%=========================================================================
% CALLER:  general purpose
% CALLEE:  sw_ptmp.m
% CARRY OUT INVERSE CALCULATION BY SWAPPING P0 & PR.*/
double PT;
PT = sw_ptmp(S,T,PR,P);
return PT;
}
/*%=========================================================================
*/
typedef double pixelvalue ;
#define PIX_SORT(a,b) { if ((a)>(b)) PIX_SWAP((a),(b)); }
#define PIX_SWAP(a,b) { pixelvalue temp=(a);(a)=(b);(b)=temp; }
/*----------------------------------------------------------------------------
   Function :   opt_med5()
   In       :   pointer to array of 5 pixel values
   Out      :   a pixelvalue
   Job      :   optimized search of the median of 5 pixel values
   Notice   :   found on sci.image.processing
                cannot go faster unless assumptions are made
                on the nature of the input signal.
 ---------------------------------------------------------------------------*/
pixelvalue opt_med5(pixelvalue * p)
{
    PIX_SORT(p[0],p[1]) ; PIX_SORT(p[3],p[4]) ; PIX_SORT(p[0],p[3]) ;
    PIX_SORT(p[1],p[4]) ; PIX_SORT(p[1],p[2]) ; PIX_SORT(p[2],p[3]) ;
    PIX_SORT(p[1],p[2]) ; return(p[2]) ;
}

/* ==============================================*/
double ButterLowCoeff(double Tfilt, struct butter * B){
/* returns coefficients of second order low pass butterworth filter
with next tabulated pass time greater than or equal to Tfilt
or maximum (11.3 hr).
Coefficients are returned in butterworth filter structure 
These coefficients & code are generated by a matlab program
*/

# define NBUTTER 10
static double A2s[NBUTTER] ={
-1.8521464854e+00, -1.8953687811e+00, -1.9259839697e+00,
-1.9476516262e+00, -1.9629800894e+00, -1.9738215275e+00,
-1.9814885091e+00, -1.9869102157e+00, -1.9907440595e+00,
-1.9934550386e+00 };  

static double A3s[NBUTTER] ={
8.6234862603e-01, 9.0057694026e-01, 9.2862708612e-01,
9.4898728849e-01, 9.6365298422e-01, 9.7415978479e-01,
9.8165828262e-01, 9.8699533166e-01, 9.9078669884e-01,
9.9347638707e-01 }; 

static double B1s[NBUTTER] ={
2.5505351585e-03, 1.3020397991e-03, 6.6077909823e-04,
3.3391556065e-04, 1.6822370859e-04, 8.4564321528e-05,
4.2443368140e-05, 2.1278990733e-05, 1.0659834541e-05,
5.3371278771e-06 };

static double B2s[NBUTTER] ={
5.1010703171e-03, 2.6040795981e-03, 1.3215581965e-03,
6.6783112129e-04, 3.3644741718e-04, 1.6912864306e-04,
8.4886736280e-05, 4.2557981466e-05, 2.1319669081e-05,
1.0674255754e-05 }; 

static double B3s[NBUTTER] ={
2.5505351585e-03, 1.3020397991e-03, 6.6077909823e-04,
3.3391556065e-04, 1.6822370859e-04, 8.4564321528e-05,
4.2443368140e-05, 2.1278990733e-05, 1.0659834541e-05,
5.3371278771e-06 }; 

static double Ts[NBUTTER] ={
1800.000000, 2545.584412, 3600.000000,
5091.168825, 7200.000000, 10182.337649,
14400.000000, 20364.675298, 28800.000000,
40729.350596 }; 

int i;
for ( i=0; i<NBUTTER;i++) {
	if (Tfilt <=Ts[i] )break;
}
if (i > NBUTTER-1)i=NBUTTER-1;

B->Tfilt=Ts[i];
B->A2=A2s[i];
B->A3=A3s[i];
B->B1=B1s[i];
B->B2=B2s[i];
B->B3=B3s[i];

return Ts[i];
}

/* ==============================================*/
double ButterHiCoeff(double Tfilt, struct butter * B){
/* returns coefficients of second order high pass butterworth filter
with next tabulated pass time greater than or equal to Tfilt
or maximum (11.3 hr).
Coefficients are returned in butterworth filter structure 
These coefficients & code are generated by a matlab program
*/
# define NBUTTER 10
static double A2s[NBUTTER] ={
-1.8521464854e+00, -1.8953687811e+00, -1.9259839697e+00,
-1.9476516262e+00, -1.9629800894e+00, -1.9738215275e+00,
-1.9814885091e+00, -1.9869102157e+00, -1.9907440595e+00,
-1.9934550386e+00}; 

static double A3s[NBUTTER] ={
8.6234862603e-01, 9.0057694026e-01, 9.2862708612e-01,
9.4898728849e-01, 9.6365298422e-01, 9.7415978479e-01,
9.8165828262e-01, 9.8699533166e-01, 9.9078669884e-01,
9.9347638707e-01 };

static double B1s[NBUTTER] ={
9.2862377786e-01, 9.4898643033e-01, 9.6365276396e-01,
9.7415972868e-01, 9.8165826840e-01, 9.8699532807e-01,
9.9078669794e-01, 9.9347638684e-01, 9.9538268959e-01,
9.9673285641e-01}; 

static double B2s[NBUTTER] ={
-1.8572475557e+00, -1.8979728607e+00, -1.9273055279e+00,
-1.9483194574e+00, -1.9633165368e+00, -1.9739906561e+00,
-1.9815733959e+00, -1.9869527737e+00, -1.9907653792e+00,
-1.9934657128e+00};  

static double B3s[NBUTTER] ={
9.2862377786e-01, 9.4898643033e-01, 9.6365276396e-01,
9.7415972868e-01, 9.8165826840e-01, 9.8699532807e-01,
9.9078669794e-01, 9.9347638684e-01, 9.9538268959e-01,
9.9673285641e-01};

static double Ts[NBUTTER] ={
1800.000000, 2545.584412, 3600.000000,
5091.168825, 7200.000000, 10182.337649,
14400.000000, 20364.675298, 28800.000000,
40729.350596}; 

int i;
for ( i=0; i<NBUTTER;i++) {
	if (Tfilt <=Ts[i] )break;
}
if (i > NBUTTER-1)i=NBUTTER-1;

B->Tfilt=Ts[i];
B->A2=A2s[i];
B->A3=A3s[i];
B->B1=B1s[i];
B->B2=B2s[i];
B->B3=B3s[i];

return Ts[i];
}


