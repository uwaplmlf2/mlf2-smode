/* arch-tag: 313d3888-919e-4dd9-8e90-684e1630ba27 */
/* MLB MISSION - Mixed Layer Base Equatorial
Same as Pycnocline but with automatic MLB detection
*/
/* PARAMETERS SET IN InitializeMLB.c  */

int next_mode(int nmode, double day_time)
{
    double x;
    static int icall=0;
    int oldmode;
	
     oldmode=nmode;
     if (nmode==MODE_START){
	log_event("next_mode:MLB Mission Start\n");
	nmode=MODE_PROFILE_UP;
	icall=1;
	return(nmode);
    } 
    else if (nmode==MODE_ERROR){ /* this should not happen */
	log_event("ERROR: next_mode ERROR MODE at wierd time- reset loop at COMM\n");
	icall=1;  /* reset loop */	
	}
    else if (nmode==MODE_COMM && icall%6+1 !=3 ){  /* Error or Return from error */
	log_event("ERROR: next_mode Comm at wrong time - reset mode loop mode\n");
	icall=2;  /* reset loop */
	}

    /* j cycles [1-4] */
    switch(icall%6 +1){
	case 1: nmode=MODE_PROFILE_UP;break;
	case 2: nmode=MODE_COMM;break;
	case 3: nmode=MODE_PROFILE_UP;break;
        case 4: nmode=MODE_PROFILE_DOWN;
		if (Mlb.go==1) {  /* do MLB computation */
			Mlb.record=1;  /* start recording */
			Mlb.point=0;  /* zero buffer */
		}
		break;
        case 5: nmode=MODE_SETTLE;  /* Do settle */
		Mlb.record=0;  /*stop recording */
		if (Mlb.go==1) {
			pMlb=&Mlb;
			x=getmlb(pMlb);  /* Get new target from saved profile */
			if (x>0){
				Drift.iso_target=x+Mlb.Sigoff;
				Ballast.rho0=Drift.iso_target;
				log_event("New  Sig_MLB %6.3f target %6.3f\n",x-1000.,Drift.iso_target-1000.);
			}
			else {
				log_event("ERROR: next_mode No new target\n");
				}
		}
		break;
        case 6: nmode=MODE_DRIFT_SEEK;
		break;
       default: nmode=MODE_ERROR;break;  /* can't get here */
    }
    log_event("next_mode: %d ->%d\n",oldmode,nmode);
    ++icall;
    return(nmode);
}



