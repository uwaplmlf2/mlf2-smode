/* This file is included in ballast.c */
/* It sets initial values of all control variables, so that the main code
	does not need to be modified when values change */
/* Do not change this variable */
static const char *__id__ = "$Id$";

/* Feb 2014 - OMZ */
// 25 July 2012 - separate parameter declaration (global) and initialization(in setup() function), in anticipation of the two-tier parameter model



# define Nav  100         /*Size of averaging array in Settle, Settle.nav must be smaller than this*/
# define RHOMIN 900  /* minimum allowed density */
# define NLOG     100   /* output data every NLOG calls  */
# define NCOMMAND 3 /* Number of real commands  1 2 3 ... NCOMMAND */
# define LOGDAYSSIM 10  /* Stop ballastlog after this many days in simulation */

/* Misc. global scalar parameters - all satellite settable */
static double Mass0 = 54.501;  /* initial mass */
static double Creep=0.00e-6;  /* kg/day */
static double deep_control = 25.e-6;
static double bottom_P=450.;  /* 180 Depth to push out piston */
static double error_P=480.; /* 220 Depth to declare emergency */
static double top_P=-100;   /* Minimum allowed depth */
static double shallow_control=3e-6;  /*  m^2/dbar */
static short int stage=0;   /* Master mode control variable */
static short int newstage=0;  /* set this by satellite to next desired stage, for orderly change at end of present stage */
static double telem_Step=0.1;   /* Commanded change in target */
static double Home_days=1;    /* Home about this often */
static short Commskipmax=3; /* maximum number of comms that can be skipped by timers - a safety feature */
static double MaxQuietDays=5; /*maximum number of days without a comm mode - ERROR - another safety feature */ 
static double SSAL_off_depth = 1; // disable SSAL and re-enable regular CTD at this depth (unless in UP2 mode) 
                            // This turns off SSAL on a down after a COM
                            // Note, that SSAL will likely be already off on a timeout, so this is mostly about re-enabling top CTD.
static double  PgtdCont=3;   /* GTD to continuous pumping shallower than this */

/**************************************************************************************************
	Declare global mission-specific parameters - Tier II
   These determine the parameters of Modes comprising particular stages
****************************************************************************************************/

static struct psballast {
	double depth1;	// Depth of the first (captive) dive & drift
	double settle1_time;	// length of the first (captive) settle - typically, very short (was: Steps.time0)
	double drift1_time;	// length of the first (captive) drift - typically, very short (was: Drift.time2)

	double depth2;	// Depth of the second (longer) dive & drift
	double settle2_time;	// length of the second (longer) settle (was: Steps.time1)
	double settle2_seektime;	// length of seeking time during settle
	double drift2_time;	// length of the second (longer) drift. NB: Vset==1 for this drift! (was: Steps.time5)
} PSBallast;


static struct steps{
	// consists of up-comm-slowdown-down settles cycle:
	struct up Up;
	struct down SlowDown;
	struct down Down;
	// and 4 settles (which can be different!)
    struct settle Settle[8];
	//double z[4];       // Settle depths,  mapped to Steps.z1-4 in Ptable
    //double time[4];	   // Settle durations, mapped to Steps.time1-4 in Ptable
	// double decay_ratio;  // Not needed here - set appropriate (fractional) Settle.decay_time /* usually settle.decay_time=steps.decay_ratio*settle.seek_time */
} Steps; // Rename to Stage 1 eventually?

static struct turb{   // NOT USED IN THIS MISSION _ JUST FOR HISTORY
	// consists of up-UpSurf-comm-down cycle:
	struct up Up;
	struct upsurf UpSurf;
	struct down Down;
	// another up (insertion):
	struct up Up3;
	// and a drift
	// ... but "Drift" structure actually has a lot of ballasting parameters (Air, Compress, Thermal_exp), so it is not justified to have Turb.Drift.
	// besides, there are no other "drifts" in this mission.
} Turb;

/* Initialize parameter values and link them with the Ptable */

int initialize_mission_parameters(void){
	int i;
	 /* Sampling intervals
     ** Note that for the profiles, the "sampling interval" is now
     ** used to specify the maximum time allowed for ballast adjustment
     ** during each sample.
     */
    Si[MODE_PROFILE_UP] = 10;
    Si[MODE_PROFILE_DOWN] = 10;
    Si[MODE_PROFILE_SURF] = 10;


    Si[MODE_SETTLE] = 50;
    Si[MODE_DRIFT_ISO] = 50;
    Si[MODE_DRIFT_ML] = 50;
    Si[MODE_DRIFT_SEEK] = 50;

	/************************************************************************************************
	 Initialization of Tier 1 parameters.
	 Some of them will be overriden by stage-specific parameters, but these are the defaults.
	*************************************************************************************************/

    /* This covers for both Down and SlowDown */
	Down.Pmax = 400; /* Max depth of down-leg */
    Down.timeout = 5000;   /* Max time of down-leg  */
    Down.Sigmax = 5000;     /* Maximum potential density - ends mode */
    Down.Bmin = 0;              /* m^3  Bocha setting  */
    Down.Speed = 100.;      /* target speed */
    Down.Brate = 0.;      /* 5e-8 Bocha speed */
    Down.Pspeed = 3;   /* don't control speed shallower than this */
    Down.drogue = 0;      /* drogue position: 0 closed, 1 open */

	Up.Bmax = 650.e-6;    /* max bocha setting*/
    Up.Speed = 10;   /* target speed */
    Up.Brate = 8e-8;   /* bocha rate to use for speed control */
    Up.Pend = 3;    /* Minimum Pressure - end of up-leg */
    Up.timeout = 3600; /* Max duration of up-leg   */
    Up.Speedbrake = 1; /* if 1, use drogue as speedbrake until float rises */
    Up.PHyst = 3.;     /* Hysterisis (m) allowed in speed control */
    Up.drogue = 0;    /* 0 or 1 - drogue open or closed during up */
    Up.surfacetime= 0;  /* time/sec past reaching up.Pend before end */

	Settle.secOday = -1;   /* end Settle when time passes this clock time [0 86400] (obsolete: replaced by timers) */
    Settle.timeout = 7200;         /* Max time of settle-leg/sec -  Skip settle if negative*/
    Settle.seek_time = 0.8;     /* time for active seeking, if seek_time<1, then a fraction of timeout  */
    Settle.decay_time = 0.4;      /* decay seeking over this time scale after seek_time, if decay_time<1, then a fraction of seek_time  */
    Settle.nav = 20;		    /* number of points to average to get settled volume */
    Settle.drogue_time = 300; /* time after start of settle to keep drogue open */
    Settle.beta = 4; /* pseudo compressive gain (big for stable)*/
    Settle.tau = 400;             /* seek gain (big for stable)*/
    Settle.weight_error = 1;    /* Accept seek values if Dsig*V0 is	less than this (in kg)*/
    Settle.Vol_error = 0.5e-6;       /* 0.5e-6 Accept volume if stdev of V0 is less	than this ( if <0 no volume set) */
    Settle.Ptarget = 15;      /* Target Pressure - used only with Nfake */
	Settle.Nfake = 0;		    /* Fake stratification relative to Ptarget  No effect if 0 */
    Settle.nskip = 1;           /*	(if 0, skip settle mode always)  */
    Settle.SetTarget = 0; /* How to set target: 	1- current PotDensity,2-Ballast.target,3-Drift.target, else constant*/
    Settle.Target = 1022.4;          /* Settle Target isopycnal */
    Settle.B0 = 50e-6;              /* bocha,  don't need to set */
    Settle.Bmin = 0.; /* Minimum Bocha */
    Settle.Bmax = 650.e-6; /* Maximum Bocha */

	Drift.SetTarget = 3; /*  How to set Target: 	1- current value, 2-Ballast.target, 3-Settle.target, else constant*/
    Drift.VoffZero = 1;  /* 1 to set Voff=0 at drift start, else keep value */
    Drift.median = 1;  /* 1 use 5 point median filter, 0 don't */
    Drift.timetype = 2; /* How to end Drift mode 1: Use time since end of last drift mode or mission start 2: At the given timeOday (defaults to noon = 0.5) 	Also use timeout */
    Drift.time_end_sec = -1;   /*time/seconds to end Drift mode */
    Drift.timeout_sec = 500.;	   /* Additional timeout to end mode / seconds*/
    Drift.Tref = 8;      /* reference temperature */
    Drift.Voff = 0;       /* Ballast adjustment during Drift  */
    Drift.Voffmin = -200e-6;  /*prevent negative runaway on bottom */
    Drift.Moff = 0;      /* Offset of Mass from Mass */
    Drift.Air = 15.5e-6;      /* grams of air buoyancy at surface -always used*/
    Drift.Compress = 3.1e-6;  /* float compressibility db^-1*/
    Drift.Thermal_exp = 0.722e-4;     /* float thermal expansion coeff */
    Drift.Target = 1024.0;         /* target isopycnal */
    Drift.iso_time = 7200;        /*seek time toward surface  sec  */
    Drift.seek_Pmin = -100;      /* depth range for drift seek mode*/
    Drift.seek_Pmax = -50;      /* continued */
    Drift.iso_Gamma = 1;/*Pseudo-compressibility  m^3/unit, 1 = isopycnal*/
    Drift.time2 = 600;  /* second timeout parameter -- compatibility only, DO NOT USE  */
    Drift.closed_time = 200;   /* drogue opens after this time   sec  */

	Ballast.SetTarget = 8;  /* how to set Ballast.Target	1,2,3-no changes,    4- end of good Settle 	5 - end of Drift, 6-end of Down, 7-end of Up, 	8- Value at P=Ballast.Pgoal	9- Value from GetMLB (not implemented yet) */
    Ballast.Vset = 0;    /*set V0: 0 - fixed/manual; 1 - from good settles, 	2 - from Settle and Drift end, 3 from drift end only */
	Ballast.MLsigmafilt = 0;   /* 1 to use LowPass filtered Sigma in ML, else Ballast.rho0 */
    Ballast.MLthreshold = -100;   /* Switch to ML ballasting if P<Pdev*[]     */
    Ballast.MLmin = 2;            /*        or if P<     */
	Ballast.MLtop = 2;          /* or between top and bottom */
	Ballast.MLbottom =10;
    Ballast.SEEKthreshold = -100;   /* Allow isoSEEK if P>Pdev*[]  & Pressure between Drift.seek_* limits   */
    Ballast.Offset = 0;		/* adds offset to bocha in drift mode */
    Ballast.T0 = 26.8;      /* most recent temperature at ballasted point */
    Ballast.S0 = 37.45;      /* most recent Salinity  */
    Ballast.P0 = 0;      /* most recent Pressure*/
    Ballast.rho0 = 1024.628;    /* most recent water (=float) density (used to be important)*/
    Ballast.B0 = 160.e-6;      /* most recent bocha setting at ballasted point */
    Ballast.V0 = 52955.00e-6;      /* Estimated float volume at zero bocha, at surface, ref Temp, no air -- IMPORTANT - First guess for Settle*/
    Ballast.TH0 = 26.8;     /* most recent potential temperature */
    Ballast.Vdev = 0;  /* most recent stdev of float volume estimates */
    Ballast.Pgoal = 30;  /* if STP==2, set STP at this depth */
    Ballast.Target = 1022.4; /* target isopycnal e.g. 1022 */

	CTD.which = BOTTOMCTD;  /* How to get one CTD value from two CTD's - BOTTOMCTD ,TOPCTD, MEANCTD or MAXCTD */
    CTD.BadMax = 25;    /*How many bad CTDs before error */
    CTD.Ptopmin = 2.1;      /* TopCTD not good above this pressure */
    CTD.Poffset = 0.106;      /* Offset for Pressure to be zero when top of float is at surface, adjusts pressure to be that at middle of float hull*/
    CTD.Separation = 1.42;  /* Distance between two CTDs*/
    CTD.TopSoffset = 0;    /* Correction to top CTD salinity */
    CTD.TopToffset = 0;   /*Correction to top CTD temperature*/
    CTD.BottomSoffset = 0;   /* Correction to Bottom CTD salinity */
    CTD.BottomToffset = 0;   /*Correction to Bottom CTD temperature*/

	Mlb.go = 1;  /* 1 to compute MLB target, otherwise don't */
    Mlb.record = 0;   /* 1 to record data in arrays; else don't */
    Mlb.point = 0;       /* points at next open element in raw arrays, 0-> empty */
    Mlb.Nmin = 45;    /* minimum number of data to do computation */
    Mlb.dP = 2;     /* grid spacing m */
    Mlb.dSig = 0.02;  /* bin size for Sigma search */
    Mlb.Sigoff = 0.2; /* Offset of goal from ML density, final target = MLsigma + Sigoff */
    //Mlb.Psave = 0; /* array of raw pressure */
    //Mlb.Sigsave =0 ; /* array of raw potential density */
    //Mlb.Pgrid = 0; /* array of gridded pressure */
    //Mlb.Siggrid = 0; /* array of gridded potential density */


	Error.Modes = 3;      /* 1: drift only   2: settle only 3: drift and settle   Else: None */
    Error.Pmin = 1;      /* Minimum Pressure  allowed */
    Error.Pmax = 460;     /* Maximum Pressure  allowed */
    Error.timeout = 3600;    /* time outside of this range before error is declared*/

	Bugs.start = -1;  /* sunset  / seconds of GMT day 0-86400 */
	Bugs.stop = -1;  /* sunrise */
	Bugs.start_weight = 0;  /* sunset weight / kg  */
	Bugs.stop_weight = 0;  /* sunrise weight /kg - linear interpolation between */
	Bugs.flap_interval = 10000;  /* time between flaps / seconds */
	Bugs.flap_duration = 130;  /* time between close and open */
	Bugs.weight = 0; /* weight - don't set  */

	Timer.enable = 1;	// ==1 if all timers are enabled (individual timers can be controlled by setting the time ore stage to -1
	Timer.time1 = 86000;	// time (in seconds since midnight GMT) for the first timer event. May want to change to multy-day timers eventually
	Timer.time2 = 43000;	// ... for the second timer event
	Timer.time3 = -1;	// ... for the third timer event
	Timer.time4 = -1;	// ... for the fourth timer event
	Timer.stage1 = 1;	// stage to switch to when the first timer is triggered (set to -1 to disable)
	Timer.stage2 = 1;   // ... second timer
	Timer.stage3 = 1;   // ... third timer
	Timer.stage4 = 1;   // ... fourth timer
    Timer.nskip1 = 0;   // How many timer triggers to skip?
    Timer.nskip2 = 0;   // timer2
    Timer.nskip3 = 0;   // timer3
    Timer.nskip4 = 0;   // timer4
    
    Timer.countdown1=Timer.nskip1; // DON'T SET - internal countdown variable
    Timer.countdown2=Timer.nskip2; // DON'T SET - internal countdown variable
    Timer.countdown3=Timer.nskip3; // DON'T SET - internal countdown variable
    Timer.countdown4=Timer.nskip4; // DON'T SET - internal countdown variable
    

	/************************************************************************************************
	 Initialization of Tier 2 parameters.
	 These determine variations of the Mode parameters above for each particular stage
	*************************************************************************************************/

	PSBallast.depth1 = 60;	// Depth of the first (captive) dive & drift
	PSBallast.settle1_time = 300;	// length of the first (captive) settle - typically, very short (was: Steps.time0)
	PSBallast.drift1_time = 100;	// length of the first (captive) drift - typically, very short (was: Drift.time2)
	PSBallast.depth2 = 200;	// Depth of the second (longer) dive & drift
	PSBallast.settle2_time = 10000;	// length of the second (longer) settle (was: Steps.time1)
	PSBallast.settle2_seektime = 8000;	// length of seeking time during settle
	PSBallast.drift2_time = 1000;	// length of the second (longer) drift. NB: Vset==1 for this drift! (was: Steps.time5)

	/* Steps parameters */
	Steps.Up = Up;             // Start with the default Up, UpSurf and Down
	Steps.Down = Down;
    
    Steps.SlowDown=Down;        /* Only list the special SlowDown parameters */
    Steps.SlowDown.Pmax = 100; /* Max depth of down-leg */
    Steps.SlowDown.timeout = 4000;   /* Max time of down-leg  */
    Steps.SlowDown.Speed = 0.08;      /* target speed */
    Steps.SlowDown.Brate = 5e-8;      /* 5e-8 Bocha speed */
        
    Steps.Down.Pmax=1.;   // This is automatically increased to max of Steps.Settle[].Ptarget + 1
    
	Steps.Settle[0] = Settle;    // Copy Settle to Steps.Settle
	Steps.Settle[1] = Settle;   // Don't change
	Steps.Settle[2] = Settle;   // Don't change
	Steps.Settle[3] = Settle;   // Don't change
    Steps.Settle[4] = Settle;   // Don't change
	Steps.Settle[5] = Settle;   // Don't change
	Steps.Settle[6] = Settle;   // Don't change
	Steps.Settle[7] = Settle;   // Don't change
    
    Steps.Settle[0].drogue_time=0;   // No need to open drogue at start of each step
    Steps.Settle[1].drogue_time=0;
    Steps.Settle[2].drogue_time=0;
    Steps.Settle[3].drogue_time=0;
    Steps.Settle[4].drogue_time=0;
    Steps.Settle[5].drogue_time=0;
    Steps.Settle[6].drogue_time=0;
    Steps.Settle[7].drogue_time=0;
	Steps.Settle[0].Ptarget = 400;       // First settle depth,  mapped to Steps.z1 in Ptable    
    Steps.Settle[1].Ptarget = 200;		 // second,  mapped to Steps.z2
    Steps.Settle[2].Ptarget = 100;		 // etc.
    Steps.Settle[3].Ptarget = -100;   
    Steps.Settle[4].Ptarget = 175;   
    Steps.Settle[5].Ptarget = 150;       
    Steps.Settle[6].Ptarget = 125;       
    Steps.Settle[7].Ptarget = 100.;        /* surface measurement here */
    Steps.Settle[0].timeout=6000;        // Settle times
    Steps.Settle[1].timeout=12500;    
    Steps.Settle[2].timeout=8500;
    Steps.Settle[3].timeout=9500;
    Steps.Settle[4].timeout=9500;    
    Steps.Settle[5].timeout=9500;    
    Steps.Settle[6].timeout=9500;    
    Steps.Settle[7].timeout=9500;    
    
	/* Stage 2 (Turbulence) parameters    NOT USED HERE */
	Turb.Up = Up;
	Turb.UpSurf = UpSurf;
	Turb.Down = Down;
	Turb.Up3 =  Up; // Danger! Up3 is just another UP.  - Special Insertion Up

	Turb.Down.Pmax = 40; // Insertion depth + about 10m
	Turb.Down.Bmin = 0.e-6;

    Turb.Up3.surfacetime = 0;
	Turb.Up3.Speed = 0.01;       // target speed 
    Turb.Up3.PHyst = 0;
	Turb.Up3.timeout = 300;      //  Adjusted to get float to the right place before ending up
	Turb.Up3.Bmax = 650e-6;
	Turb.Up3.Pend = 30;            // Insertion depth


  /* Make Ptable links to the Mission-specific (Tier 2) parameters */


    /* PS Ballast */
    add_param("psballast.depth1",		PTYPE_DOUBLE, &PSBallast.depth1);
    add_param("psballast.settle1_time",	PTYPE_DOUBLE, &PSBallast.settle1_time);
    add_param("psballast.drift1_time",	PTYPE_DOUBLE, &PSBallast.drift1_time);

    add_param("psballast.depth2",		PTYPE_DOUBLE, &PSBallast.depth2);
    add_param("psballast.settle2_time",	PTYPE_DOUBLE, &PSBallast.settle2_time);
    add_param("psballast.settle2_seektime",	PTYPE_DOUBLE, &PSBallast.settle2_seektime);
    add_param("psballast.drift2_time",	PTYPE_DOUBLE, &PSBallast.drift2_time);
	/* Steps (Stage 1) */

    add_param("commskipmax",	PTYPE_SHORT,  &Commskipmax );
    add_param("pgtdcont",	PTYPE_DOUBLE,  &PgtdCont );

    add_param("steps.down.Pmax",		PTYPE_DOUBLE, &Steps.Down.Pmax);
    add_param("steps.down.timeout",	PTYPE_DOUBLE, &Steps.Down.timeout);
    add_param("steps.down.Sigmax",	PTYPE_DOUBLE, &Steps.Down.Sigmax);
    add_param("steps.Down.Bmin",		PTYPE_DOUBLE, &Steps.Down.Bmin);
    add_param("steps.down.Speed",		PTYPE_DOUBLE, &Steps.Down.Speed);
    add_param("steps.down.Brate",		PTYPE_DOUBLE, &Steps.Down.Brate);
    add_param("steps.down.Pspeed",    PTYPE_DOUBLE, &Steps.Down.Pspeed);
    add_param("steps.down.drogue",	PTYPE_SHORT,  &Steps.Down.drogue);
    
    add_param("steps.slowdown.Pmax",		PTYPE_DOUBLE, &Steps.SlowDown.Pmax);
    add_param("steps.slowdown.timeout",	PTYPE_DOUBLE, &Steps.SlowDown.timeout);
    add_param("steps.slowdown.Sigmax",	PTYPE_DOUBLE, &Steps.SlowDown.Sigmax);
    add_param("steps.slowdown.Bmin",		PTYPE_DOUBLE, &Steps.SlowDown.Bmin);
    add_param("steps.slowdown.Speed",		PTYPE_DOUBLE, &Steps.SlowDown.Speed);
    add_param("steps.slowdown.Brate",		PTYPE_DOUBLE, &Steps.SlowDown.Brate);
    add_param("steps.slowdown.Pspeed",    PTYPE_DOUBLE, &Steps.SlowDown.Pspeed);
    add_param("steps.slowdown.drogue",	PTYPE_SHORT,  &Steps.SlowDown.drogue);

    add_param("steps.up.Bmax",			PTYPE_DOUBLE, &Steps.Up.Bmax);
    add_param("steps.up.Speed",		PTYPE_DOUBLE, &Steps.Up.Speed);
    add_param("steps.up.Brate",			PTYPE_DOUBLE, &Steps.Up.Brate);
    add_param("steps.up.Pend",			PTYPE_DOUBLE, &Steps.Up.Pend);
    add_param("steps.up.timeout",		PTYPE_DOUBLE, &Steps.Up.timeout);
    add_param("steps.up.Speedbrake",	PTYPE_SHORT,  &Steps.Up.Speedbrake);
    add_param("steps.up.PHyst",			PTYPE_DOUBLE, &Steps.Up.PHyst);
    add_param("steps.up.drogue",		PTYPE_SHORT,  &Steps.Up.drogue);
    add_param("steps.up.surfacetime",	PTYPE_DOUBLE, &Steps.Up.surfacetime);

    add_param("steps.settle1.secOday", PTYPE_DOUBLE, &Steps.Settle[0].secOday);
    add_param("steps.settle1.timeout",	PTYPE_DOUBLE, &Steps.Settle[0].timeout);
    add_param("steps.settle1.seek_time",	PTYPE_DOUBLE, &Steps.Settle[0].seek_time);
    add_param("steps.settle1.decay_time",	PTYPE_DOUBLE, &Steps.Settle[0].decay_time);
    add_param("steps.settle1.nav",		PTYPE_SHORT, &Steps.Settle[0].nav);
    add_param("steps.settle1.drogue_time",	PTYPE_DOUBLE, &Steps.Settle[0].drogue_time);
    add_param("steps.settle1.beta",		PTYPE_DOUBLE, &Steps.Settle[0].beta);
    add_param("steps.settle1.tau",			PTYPE_DOUBLE, &Steps.Settle[0].tau);
    add_param("steps.settle1.weight_error",	PTYPE_DOUBLE, &Steps.Settle[0].weight_error);
    add_param("steps.settle1.Vol_error",	PTYPE_DOUBLE, &Steps.Settle[0].Vol_error);
    add_param("steps.settle1.Ptarget",	PTYPE_DOUBLE, &Steps.Settle[0].Ptarget);
    add_param("steps.settle1.Nfake",	PTYPE_DOUBLE, &Steps.Settle[0].Nfake);
    add_param("steps.settle1.nskip",		PTYPE_SHORT, &Steps.Settle[0].nskip);
    add_param("steps.settle1.SetTarget",	PTYPE_SHORT, &Steps.Settle[0].SetTarget);
    add_param("steps.settle1.Target",		PTYPE_DOUBLE, &Steps.Settle[0].Target);
    add_param("steps.settle1.B0",			PTYPE_DOUBLE, &Steps.Settle[0].B0);
    add_param("steps.settle1.Bmin",		PTYPE_DOUBLE, &Steps.Settle[0].Bmin);
    add_param("steps.settle1.Bmax",		PTYPE_DOUBLE, &Steps.Settle[0].Bmax);

	add_param("steps.settle2.secOday", PTYPE_DOUBLE, &Steps.Settle[1].secOday);
    add_param("steps.settle2.timeout",	PTYPE_DOUBLE, &Steps.Settle[1].timeout);
    add_param("steps.settle2.seek_time",	PTYPE_DOUBLE, &Steps.Settle[1].seek_time);
    add_param("steps.settle2.decay_time",	PTYPE_DOUBLE, &Steps.Settle[1].decay_time);
    add_param("steps.settle2.nav",		PTYPE_SHORT, &Steps.Settle[1].nav);
    add_param("steps.settle2.drogue_time",	PTYPE_DOUBLE, &Steps.Settle[1].drogue_time);
    add_param("steps.settle2.beta",		PTYPE_DOUBLE, &Steps.Settle[1].beta);
    add_param("steps.settle2.tau",			PTYPE_DOUBLE, &Steps.Settle[1].tau);
    add_param("steps.settle2.weight_error",	PTYPE_DOUBLE, &Steps.Settle[1].weight_error);
    add_param("steps.settle2.Vol_error",	PTYPE_DOUBLE, &Steps.Settle[1].Vol_error);
    add_param("steps.settle2.Ptarget",	PTYPE_DOUBLE, &Steps.Settle[1].Ptarget);
    add_param("steps.settle2.Nfake",	PTYPE_DOUBLE, &Steps.Settle[1].Nfake);
    add_param("steps.settle2.nskip",		PTYPE_SHORT, &Steps.Settle[1].nskip);
    add_param("steps.settle2.SetTarget",	PTYPE_SHORT, &Steps.Settle[1].SetTarget);
    add_param("steps.settle2.Target",		PTYPE_DOUBLE, &Steps.Settle[1].Target);
    add_param("steps.settle2.B0",			PTYPE_DOUBLE, &Steps.Settle[1].B0);
    add_param("steps.settle2.Bmin",		PTYPE_DOUBLE, &Steps.Settle[1].Bmin);
    add_param("steps.settle2.Bmax",		PTYPE_DOUBLE, &Steps.Settle[1].Bmax);

	add_param("steps.settle3.secOday", PTYPE_DOUBLE, &Steps.Settle[2].secOday);
    add_param("steps.settle3.timeout",	PTYPE_DOUBLE, &Steps.Settle[2].timeout);
    add_param("steps.settle3.seek_time",	PTYPE_DOUBLE, &Steps.Settle[2].seek_time);
    add_param("steps.settle3.decay_time",	PTYPE_DOUBLE, &Steps.Settle[2].decay_time);
    add_param("steps.settle3.nav",		PTYPE_SHORT, &Steps.Settle[2].nav);
    add_param("steps.settle3.drogue_time",	PTYPE_DOUBLE, &Steps.Settle[2].drogue_time);
    add_param("steps.settle3.beta",		PTYPE_DOUBLE, &Steps.Settle[2].beta);
    add_param("steps.settle3.tau",			PTYPE_DOUBLE, &Steps.Settle[2].tau);
    add_param("steps.settle3.weight_error",	PTYPE_DOUBLE, &Steps.Settle[2].weight_error);
    add_param("steps.settle3.Vol_error",	PTYPE_DOUBLE, &Steps.Settle[2].Vol_error);
    add_param("steps.settle3.Ptarget",	PTYPE_DOUBLE, &Steps.Settle[2].Ptarget);
    add_param("steps.settle3.Nfake",	PTYPE_DOUBLE, &Steps.Settle[2].Nfake);
    add_param("steps.settle3.nskip",		PTYPE_SHORT, &Steps.Settle[2].nskip);
    add_param("steps.settle3.SetTarget",	PTYPE_SHORT, &Steps.Settle[2].SetTarget);
    add_param("steps.settle3.Target",		PTYPE_DOUBLE, &Steps.Settle[2].Target);
    add_param("steps.settle3.B0",			PTYPE_DOUBLE, &Steps.Settle[2].B0);
    add_param("steps.settle3.Bmin",		PTYPE_DOUBLE, &Steps.Settle[2].Bmin);
    add_param("steps.settle3.Bmax",		PTYPE_DOUBLE, &Steps.Settle[2].Bmax);

	add_param("steps.settle4.secOday", PTYPE_DOUBLE, &Steps.Settle[3].secOday);
    add_param("steps.settle4.timeout",	PTYPE_DOUBLE, &Steps.Settle[3].timeout);
    add_param("steps.settle4.seek_time",	PTYPE_DOUBLE, &Steps.Settle[3].seek_time);
    add_param("steps.settle4.decay_time",	PTYPE_DOUBLE, &Steps.Settle[3].decay_time);
    add_param("steps.settle4.nav",		PTYPE_SHORT, &Steps.Settle[3].nav);
    add_param("steps.settle4.drogue_time",	PTYPE_DOUBLE, &Steps.Settle[3].drogue_time);
    add_param("steps.settle4.beta",		PTYPE_DOUBLE, &Steps.Settle[3].beta);
    add_param("steps.settle4.tau",			PTYPE_DOUBLE, &Steps.Settle[3].tau);
    add_param("steps.settle4.weight_error",	PTYPE_DOUBLE, &Steps.Settle[3].weight_error);
    add_param("steps.settle4.Vol_error",	PTYPE_DOUBLE, &Steps.Settle[3].Vol_error);
    add_param("steps.settle4.Ptarget",	PTYPE_DOUBLE, &Steps.Settle[3].Ptarget);
    add_param("steps.settle4.Nfake",	PTYPE_DOUBLE, &Steps.Settle[3].Nfake);
    add_param("steps.settle4.nskip",		PTYPE_SHORT, &Steps.Settle[3].nskip);
    add_param("steps.settle4.SetTarget",	PTYPE_SHORT, &Steps.Settle[3].SetTarget);
    add_param("steps.settle4.Target",		PTYPE_DOUBLE, &Steps.Settle[3].Target);
    add_param("steps.settle4.B0",			PTYPE_DOUBLE, &Steps.Settle[3].B0);
    add_param("steps.settle4.Bmin",		PTYPE_DOUBLE, &Steps.Settle[3].Bmin);
    add_param("steps.settle4.Bmax",		PTYPE_DOUBLE, &Steps.Settle[3].Bmax);
    
    add_param("steps.settle5.secOday", PTYPE_DOUBLE, &Steps.Settle[4].secOday);
    add_param("steps.settle5.timeout",	PTYPE_DOUBLE, &Steps.Settle[4].timeout);
    add_param("steps.settle5.seek_time",	PTYPE_DOUBLE, &Steps.Settle[4].seek_time);
    add_param("steps.settle5.decay_time",	PTYPE_DOUBLE, &Steps.Settle[4].decay_time);
    add_param("steps.settle5.nav",		PTYPE_SHORT, &Steps.Settle[4].nav);
    add_param("steps.settle5.drogue_time",	PTYPE_DOUBLE, &Steps.Settle[4].drogue_time);
    add_param("steps.settle5.beta",		PTYPE_DOUBLE, &Steps.Settle[4].beta);
    add_param("steps.settle5.tau",			PTYPE_DOUBLE, &Steps.Settle[4].tau);
    add_param("steps.settle5.weight_error",	PTYPE_DOUBLE, &Steps.Settle[4].weight_error);
    add_param("steps.settle5.Vol_error",	PTYPE_DOUBLE, &Steps.Settle[4].Vol_error);
    add_param("steps.settle5.Ptarget",	PTYPE_DOUBLE, &Steps.Settle[4].Ptarget);
    add_param("steps.settle5.Nfake",	PTYPE_DOUBLE, &Steps.Settle[4].Nfake);
    add_param("steps.settle5.nskip",		PTYPE_SHORT, &Steps.Settle[4].nskip);
    add_param("steps.settle5.SetTarget",	PTYPE_SHORT, &Steps.Settle[4].SetTarget);
    add_param("steps.settle5.Target",		PTYPE_DOUBLE, &Steps.Settle[4].Target);
    add_param("steps.settle5.B0",			PTYPE_DOUBLE, &Steps.Settle[4].B0);
    add_param("steps.settle5.Bmin",		PTYPE_DOUBLE, &Steps.Settle[4].Bmin);
    add_param("steps.settle5.Bmax",		PTYPE_DOUBLE, &Steps.Settle[4].Bmax);

    add_param("steps.settle6.secOday", PTYPE_DOUBLE, &Steps.Settle[5].secOday);
    add_param("steps.settle6.timeout",	PTYPE_DOUBLE, &Steps.Settle[5].timeout);
    add_param("steps.settle6.seek_time",	PTYPE_DOUBLE, &Steps.Settle[5].seek_time);
    add_param("steps.settle6.decay_time",	PTYPE_DOUBLE, &Steps.Settle[5].decay_time);
    add_param("steps.settle6.nav",		PTYPE_SHORT, &Steps.Settle[5].nav);
    add_param("steps.settle6.drogue_time",	PTYPE_DOUBLE, &Steps.Settle[5].drogue_time);
    add_param("steps.settle6.beta",		PTYPE_DOUBLE, &Steps.Settle[5].beta);
    add_param("steps.settle6.tau",			PTYPE_DOUBLE, &Steps.Settle[5].tau);
    add_param("steps.settle6.weight_error",	PTYPE_DOUBLE, &Steps.Settle[5].weight_error);
    add_param("steps.settle6.Vol_error",	PTYPE_DOUBLE, &Steps.Settle[5].Vol_error);
    add_param("steps.settle6.Ptarget",	PTYPE_DOUBLE, &Steps.Settle[5].Ptarget);
    add_param("steps.settle6.Nfake",	PTYPE_DOUBLE, &Steps.Settle[5].Nfake);
    add_param("steps.settle6.nskip",		PTYPE_SHORT, &Steps.Settle[5].nskip);
    add_param("steps.settle6.SetTarget",	PTYPE_SHORT, &Steps.Settle[5].SetTarget);
    add_param("steps.settle6.Target",		PTYPE_DOUBLE, &Steps.Settle[5].Target);
    add_param("steps.settle6.B0",			PTYPE_DOUBLE, &Steps.Settle[5].B0);
    add_param("steps.settle6.Bmin",		PTYPE_DOUBLE, &Steps.Settle[5].Bmin);
    add_param("steps.settle6.Bmax",		PTYPE_DOUBLE, &Steps.Settle[5].Bmax);
    
    add_param("steps.settle7.secOday", PTYPE_DOUBLE, &Steps.Settle[6].secOday);
    add_param("steps.settle7.timeout",	PTYPE_DOUBLE, &Steps.Settle[6].timeout);
    add_param("steps.settle7.seek_time",	PTYPE_DOUBLE, &Steps.Settle[6].seek_time);
    add_param("steps.settle7.decay_time",	PTYPE_DOUBLE, &Steps.Settle[6].decay_time);
    add_param("steps.settle7.nav",		PTYPE_SHORT, &Steps.Settle[6].nav);
    add_param("steps.settle7.drogue_time",	PTYPE_DOUBLE, &Steps.Settle[6].drogue_time);
    add_param("steps.settle7.beta",		PTYPE_DOUBLE, &Steps.Settle[6].beta);
    add_param("steps.settle7.tau",			PTYPE_DOUBLE, &Steps.Settle[6].tau);
    add_param("steps.settle7.weight_error",	PTYPE_DOUBLE, &Steps.Settle[6].weight_error);
    add_param("steps.settle7.Vol_error",	PTYPE_DOUBLE, &Steps.Settle[6].Vol_error);
    add_param("steps.settle7.Ptarget",	PTYPE_DOUBLE, &Steps.Settle[6].Ptarget);
    add_param("steps.settle7.Nfake",	PTYPE_DOUBLE, &Steps.Settle[6].Nfake);
    add_param("steps.settle7.nskip",		PTYPE_SHORT, &Steps.Settle[6].nskip);
    add_param("steps.settle7.SetTarget",	PTYPE_SHORT, &Steps.Settle[6].SetTarget);
    add_param("steps.settle7.Target",		PTYPE_DOUBLE, &Steps.Settle[6].Target);
    add_param("steps.settle7.B0",			PTYPE_DOUBLE, &Steps.Settle[6].B0);
    add_param("steps.settle7.Bmin",		PTYPE_DOUBLE, &Steps.Settle[6].Bmin);
    add_param("steps.settle7.Bmax",		PTYPE_DOUBLE, &Steps.Settle[6].Bmax);

    add_param("steps.settle8.secOday", PTYPE_DOUBLE, &Steps.Settle[7].secOday);
    add_param("steps.settle8.timeout",	PTYPE_DOUBLE, &Steps.Settle[7].timeout);
    add_param("steps.settle8.seek_time",	PTYPE_DOUBLE, &Steps.Settle[7].seek_time);
    add_param("steps.settle8.decay_time",	PTYPE_DOUBLE, &Steps.Settle[7].decay_time);
    add_param("steps.settle8.nav",		PTYPE_SHORT, &Steps.Settle[7].nav);
    add_param("steps.settle8.drogue_time",	PTYPE_DOUBLE, &Steps.Settle[7].drogue_time);
    add_param("steps.settle8.beta",		PTYPE_DOUBLE, &Steps.Settle[7].beta);
    add_param("steps.settle8.tau",			PTYPE_DOUBLE, &Steps.Settle[7].tau);
    add_param("steps.settle8.weight_error",	PTYPE_DOUBLE, &Steps.Settle[7].weight_error);
    add_param("steps.settle8.Vol_error",	PTYPE_DOUBLE, &Steps.Settle[7].Vol_error);
    add_param("steps.settle8.Ptarget",	PTYPE_DOUBLE, &Steps.Settle[7].Ptarget);
    add_param("steps.settle8.Nfake",	PTYPE_DOUBLE, &Steps.Settle[7].Nfake);
    add_param("steps.settle8.nskip",		PTYPE_SHORT, &Steps.Settle[7].nskip);
    add_param("steps.settle8.SetTarget",	PTYPE_SHORT, &Steps.Settle[7].SetTarget);
    add_param("steps.settle8.Target",		PTYPE_DOUBLE, &Steps.Settle[7].Target);
    add_param("steps.settle8.B0",			PTYPE_DOUBLE, &Steps.Settle[7].B0);
    add_param("steps.settle8.Bmin",		PTYPE_DOUBLE, &Steps.Settle[7].Bmin);
    add_param("steps.settle8.Bmax",		PTYPE_DOUBLE, &Steps.Settle[7].Bmax);

	};
