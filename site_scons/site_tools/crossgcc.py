#!/usr/bin/env python
#
# Scons script to support cross-compiling for MLF2
#

PREFIX = 'm68k-coff-'

def gen_mkbin(source, target, env, for_signature):
    return '$OBJCOPY -O binary -S %s %s' % (source[0], target[0])

def gen_mkhex(source, target, env, for_signature):
    return '$OBJCOPY -O srec -S %s %s' % (source[0], target[0])

def generate(env):
    env['CC'] = PREFIX + 'gcc'
    env['AS'] = PREFIX + 'as'
    env['AR'] = PREFIX + 'ar'
    env['LD'] = PREFIX + 'ld'
    env['RANLIB'] = PREFIX + 'ranlib'
    env['CXX'] = PREFIX + 'g++'
    env['OBJCOPY'] = PREFIX + 'objcopy'
    env['PROGSUFFIX']   = '.coff'
    env['CCFLAGS'] = '-Os -mcpu32 -mshort -fshort-enums -fno-builtin -D__TT8__ -D__PICODOS__'
    env['LINKFLAGS'] = '$CCFLAGS -T $LINKFILE'
    env['LINK'] = '$CC'
    env['LIBPREFIX'] = 'lib'
    env['LIBSUFFIX'] = '.a'
    mkbin = env.Builder(generator=gen_mkbin,
                        suffix='.run',
                        src_suffix='.coff',
                        single_source=True)
    mkhex = env.Builder(generator=gen_mkhex,
                        suffix='.rhx',
                        src_suffix='.coff',
                        single_source=True)
    mkhex2 = env.Builder(generator=gen_mkhex,
                         suffix='.ahx',
                         src_suffix='.coff',
                         single_source=True)
    env['BUILDERS']['PicoDos'] = mkbin
    env['BUILDERS']['Rhx'] = mkhex
    env['BUILDERS']['Ahx'] = mkhex2

def exists(env):
    return True
