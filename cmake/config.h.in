/*
** MLF2 configuration header file
*/
#ifndef _CONFIG_H_
#define _CONFIG_H_

/* Define if CTD+Oxygen is used */
#cmakedefine HAVE_CTDO

/* Specify the number of CTDs */
#cmakedefine HAVE_CTD @HAVE_CTD@

/* Define if bottom CTD is on the first serial port (port 5a) */
#define BOTTOM_CTD_IS_FIRST

/* Define if PAR light sensor is used */
#cmakedefine HAVE_PAR

/* Define if 490nm light sensor is used */
#cmakedefine HAVE_I490

/* Define if ADCP is used */
#cmakedefine HAVE_ADCP

/* Define if ambient noise receiver is used */
#cmakedefine HAVE_NOISE

/* Define if BB2F is used */
#cmakedefine HAVE_BB2F

/* Define if altimeter is used */
#cmakedefine HAVE_ALTIMETER

/* Define if ECO Scattering Meter is used */
#cmakedefine HAVE_ECO

/* Define if C-STAR is used */
#cmakedefine HAVE_CSTAR

/* Define if C-STAR wiper is used (connected to drogue motor) */
#cmakedefine HAVE_CSTAR_WIPER

/* Define if SBE-38 Thermometer is used */
#cmakedefine HAVE_THERM

/* Define if 1hz pressure sensor is used */
#cmakedefine HAVE_FASTPR

/* Define if ADV is used */
#cmakedefine HAVE_ADV

/* Define if fluorometer is used */
#ifndef HAVE_ECO
#undef HAVE_FLUOROMETER
#endif

#ifdef HAVE_FASTPR
#define FASTPR_ONLY
#endif

/* Define if ARGOS is used for communications */
#cmakedefine ARGOS_COMMS

/* Define for BioHeavy floats */
#cmakedefine HAVE_BIOSENSORS

/* Define for Acoustic Noise Recorder */
#cmakedefine HAVE_ANR

/* Define for Optode */
#cmakedefine HAVE_OPTODE

/* Define for FLNTU */
#cmakedefine HAVE_FLNTU

/* Define for Surface Salinity sensor */
#cmakedefine HAVE_SSAL

/* Define for Acoustic Rain Gauge */
#cmakedefine HAVE_ARG

/* Define for Biospherical QCP light sensor */
#cmakedefine HAVE_QCP

/* Define for Biospherical MCP light sensor */
#cmakedefine HAVE_MCP

/* Define for Wetlabs ECO-PAR light sensor */
#cmakedefine HAVE_ECOPAR

/* Define for the new GTD */
#cmakedefine HAVE_GTD2

/* Define for the Satlantic SUNA nitrate sensor */
#cmakedefine HAVE_SUNA

/* Define if CTDs are "standard" Seabird models */
#cmakedefine HAVE_SBECTD

/* Define for Seabird SBE-63 Oxygen sensor */
#cmakedefine HAVE_SBE63

#if HAVE_CTD == 3
#define TOTAL_CTDS      2
#else
#if HAVE_CTD > 0 && defined(HAVE_CTDO)
#define TOTAL_CTDS      2
#else
#define TOTAL_CTDS      1
#endif
#endif /* HAVE_CTD == 3 */

/* Macro to define initialization functions */
#define INITFUNC(name)\
static void name(void) __attribute__((constructor));\
static void name(void)


#endif
