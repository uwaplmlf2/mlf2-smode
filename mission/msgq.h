/*
** arch-tag: 8260635b-0c9a-4514-83f7-c1836f25d029
** Time-stamp: <2005-02-11 10:43:38 mike>
*/
#ifndef _MSGQ_H_
#define _MSGQ_H_

#define MSGLEN		64	/* maximum message length */

#define MESSAGE_FILE	"alerts.xml"

void mq_init(void);
void mq_add(const char *text, ...);


#endif /* _MSGQ_H_ */
